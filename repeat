#!/bin/bash

interval=6

# Function to display usage information
function show_usage {
  echo "Usage: $0 [--interval <seconds>] <command>"
  echo "Options:"
  echo "  --interval <seconds>  Set the interval between command executions (default: $interval seconds)"
  echo "  -h, --help            Display this help message"
}


# Function to handle SIGTERM signal
function cleanup {
  printf "\nReceived SIGTERM signal. Exiting...\n"
  exit 0
}

# Register the cleanup function for SIGTERM
trap cleanup SIGINT


# Parse command-line arguments
while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      show_usage
      exit 0
      ;;
    --interval)
      shift
      if [[ $# -eq 0 || ! "$1" =~ ^[0-9]+$ ]]; then
        printf "Error: Missing or invalid argument after --interval."
        exit 1
      fi
      interval="$1"
      shift
      ;;
    *)
      break
      ;;
  esac
done

# Check that a command has been passed in
if [ $# -eq 0 ]; then
  printf "Usage: $0 <command>"
  exit 1
fi

command_to_repeat="$@"

while true; do
  $command_to_repeat

  sleep $interval
done