# shell-scripts

## Setup

1. clone the repository to a folder
2. Add the folder to your path
3. $$PROFIT$$

## Scripts

- [Repeat](./repeat)
  Simple script for rerunning commands at an interval
  ```shell
  repeat echo "Hello"
  ```
  ```shell
  repeat --intervall 1 echo "Hello"
  ```